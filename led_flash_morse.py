#
# Morse Code LED Flasher
#
# AUTHOR: arfonzo, at gmail dot com
#
import RPi.GPIO as GPIO
import time
import sys

# Connect this GPIO pin to LED.
pin_out = 17;

# Have you got a second LED connected? Make sure to update the GPIO pin as well.
second_led = True
pin_out_secondary = 4

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin_out, GPIO.OUT)
GPIO.output(pin_out, False)
if second_led:
    GPIO.setup(pin_out_secondary, GPIO.OUT)
    GPIO.output(pin_out_secondary, False)

# Duration, in scale with morse recommendations
dur_dot = 0.2
dur_long = dur_dot * 3
dur_space_code = dur_dot
dur_space_letter = dur_dot * 3
dur_space_word = dur_dot * 7

# Send signalling messages?
send_signals = True
SIGNAL_STARTING = '-.-.-'
SIGNAL_NEW_PAGE = '.-.-.'

# Dictionary
dict_morse = {
    'A': '.-',
    'B': '-...',
    'C': '-.-.',
    'D': '-..',
    'E': '.',
    'F': '..-.',
    'G': '--.',
    'H': '....',
    'I': '..',
    'J': '.---',
    'K': '-.-',
    'L': '.-..',
    'M': '--',
    'N': '-.',
    'O': '---',
    'P': '.--.',
    'Q': '--.-',
    'R': '.-.',
    'S': '...',
    'T': '-',
    'U': '..-',
    'V': '...-',
    'W': '.--',
    'X': '-..-',
    'Y': '-.--',
    'Z': '--..',
    ' ': ' ',
    '0': '-----',
    '1': '.----',
    '2': '..---',
    '3': '...--',
    '4': '....-',
    '5': '.....',
    '6': '-....',
    '7': '--...',
    '8': '---..',
    '9': '----.',
    '.': '.-.-.-',
    ',': '--..--',
    '?': '..--..',
    "'": '.----.',
    '!': '-.-.--',
    '/': '-..-.',
    '(': '-.--.',
    ')': '-.--.-',
    '&': '.-...',
    ':': '---...',
    ';': '-.-.-.',
    '=': '-...-',
    '+': '.-.-.',
    '-': '-....-',
    '_': '..--.-',
    '"': '.-..-.',
    '$': '...-..-',
    '@': '.--.-.',
    }

def flash( pin, duration ):
    GPIO.output(pin,True)
    time.sleep(duration)
    GPIO.output(pin,False)
    return

def flash_morse( pin, code ):
    first_code = True

    for m in code:
        if first_code:
            first_code = False
        else:
            time.sleep(dur_space_code)

        if m is '.':
            flash(pin, dur_dot)
        elif m is '-':
            flash(pin, dur_long)
        elif m is ' ':
            time.sleep(dur_space_word)
        else:
            print("Error: no code found for symbol %s" % m)
            return
    
    return

def parse_flashes( str ):
    #print("Parsing: %s" % str)
    first_letter = True
    print("  <MESSAGE>")

    for letter in str:
        print("Letter: %s    Morse: %s" % (letter.upper(), dict_morse.get(letter.upper(), 'NOTFOUND')))

        if first_letter:
            first_letter = False
        else:
            if second_led:
                time.sleep(dur_space_letter/2)
                flash(pin_out_secondary, dur_space_letter/10)
                time.sleep(dur_space_letter/2)
            else:    
                time.sleep(dur_space_letter)

        # Flash morse for each character.
        flash_morse(pin_out, dict_morse.get(letter.upper(), ''))

    print("  </MESSAGE>")
    return

if len(sys.argv) > 1:

    # If a second LED is present, signal incoming message first.
    #if second_led:
    #    for x in range(0,5):
    #        GPIO.output(pin_out_secondary,True)
    #        time.sleep(0.5)
    #        GPIO.output(pin_out_secondary,False)
    #        time.sleep(0.25)
    #    time.sleep(2.5)

    if send_signals:
        print("<TRANSMISSION>")
        sys.stdout.write("  <SIGNAL_STARTING")
        sys.stdout.flush()
        pin = pin_out
        if second_led:
            pin = pin_out_secondary
        flash_morse(pin, SIGNAL_STARTING)
        print(" />")
        time.sleep(dur_space_word)

    parse_flashes(sys.argv[1])

    if send_signals:
        sys.stdout.write("  <SIGNAL_NEW_PAGE")
        sys.stdout.flush()
        pin = pin_out
        time.sleep(dur_space_word)
        if second_led:
            pin = pin_out_secondary
        flash_morse(pin, SIGNAL_NEW_PAGE)
        print(" />")

        print("</TRANSMISSION>")

else:
    print("Error: I need an argument.")

GPIO.cleanup()

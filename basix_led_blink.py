#import the GPIO and time package
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)
# loop through X times, on/off for 1 second
for i in range(10):
    GPIO.output(7,True)
    time.sleep(0.5)
    GPIO.output(7,False)
    time.sleep(0.5)
GPIO.cleanup()


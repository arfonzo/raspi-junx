#!/bin/sh
#
# Shell script to flash LED with processor and memory usage percentages.
# - Sample: P12 M34
# - Processor: 12%, Memory: 34%
#
python led_flash_morse.py "P$(top -bn 1 | awk 'NR>7{s+=$9} END {print int(s)}') M$(free | grep Mem | awk '{print int($3/$2 * 100+0.5)}')"


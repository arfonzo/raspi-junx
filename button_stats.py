#
# Display Stats on Button Press
#
import os
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)

last_input_state = 0
counter = 0
first = True;

while True:
    input_state = GPIO.input(22)

    if (input_state == False):

        if (last_input_state != input_state):
            counter += 1
            print("Button state: Pressed (%d)" % counter)

    if (input_state == True):
        if (last_input_state != input_state):
            print('Button state: Off')
            if first:
                first = False
            else:
                os.system("%s/rpi-led-stats.sh" % os.getcwd())

    last_input_state = input_state
    time.sleep(0.1)

GPIO.cleanup()
    

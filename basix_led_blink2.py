#import the GPIO and time package
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)	# GPIO4
GPIO.setup(11, GPIO.OUT) # GPIO17

# loop through X times, on/off for 1 second
for i in range(10):
    GPIO.output(7,True)
    GPIO.output(11,False)
    time.sleep(0.5)
    GPIO.output(7,False)
    GPIO.output(11,True)
    time.sleep(0.5)
GPIO.cleanup()


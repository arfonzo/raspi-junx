#
# Use a Switch/Button to control 2 LEDs:
# - One lights while button is depressed.
# - Another flashes the times button has been pressed.
#
import RPi.GPIO as GPIO
import time

pin_out_pressed = 4
pin_out = 17

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pin_out_pressed, GPIO.OUT)
GPIO.setup(pin_out, GPIO.OUT)

GPIO.output(pin_out_pressed, False)
GPIO.output(pin_out, False)

last_input_state = 0
counter = 0

def flash( duration ):
    GPIO.output(pin_out,True)
    time.sleep(duration)
    GPIO.output(pin_out,False)
    time.sleep(duration)
    return

def parse_flashes( num ):
    tens = int(num/10)
    mod = num % 10

    # Long flash every ten
    for i in range(tens):
        flash(0.6)

    # Short flash single units
    for i in range(mod):
        flash(0.2)

    return

while True:
    input_state = GPIO.input(22)

    if (input_state == False):

        if (last_input_state != input_state):
            counter += 1
            print("Button state: Pressed (%d)" % counter)

        # Turn on LED
        GPIO.output(pin_out_pressed, True)

    if (input_state == True):
        if (last_input_state != input_state):
            print('Button state: Off')
            # Turn off LED
            GPIO.output(pin_out_pressed, False)
            # Flash LED counter
            if (counter > 0):
                parse_flashes(counter)

    last_input_state = input_state
    time.sleep(0.1)

GPIO.cleanup()
    


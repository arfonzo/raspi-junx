raspi-junx
==========

A collection of raspberry pi GPIO code and other miscellaneous junk.

You should not run any of this code: it may destroy your rpi (or not). Either way, you've been warned!

`arfonzo, at gmail dot com`


Warning
-------

Do not run any of this code unless you know what you're doing:

* Always update the sources to reflect your build--i.e., GPIO pins. Failure to do so could destroy your rpi and associated accessories (i.e., if IN and OUT get mixed on the same pin by accident, if you send 5V through your rpi, or other such delightful hazards).

* Yep, that means you'll need at least a stumbling familiarity with the programming languages used.


Structure
---------

* `basix-*` Simple examples, mostly from elsewhere to get started.

* `button_stats.py` Press a button, morse code LED shows processor and memory usage percentages.

* `led_flash_morse.py` Flash LEDs with a message in morse code.

* `rpi-led-*.sh` Shell scripts for usage with *led_flash_morse.py*.


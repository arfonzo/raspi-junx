#
# Flash LED counter
# - takes number to represent in flashes as the only argument
# - Example: 192 will result in 19 long flashes, 2 short.
#
# AUTHOR: arfonzo, at gmail dot com
#
import RPi.GPIO as GPIO
import time
import sys

# Connect this GPIO pin to LED.
pin_out = 17;

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(pin_out, GPIO.OUT)
GPIO.output(pin_out, False)

def flash( duration ):
    GPIO.output(pin_out,True)
    time.sleep(duration)
    GPIO.output(pin_out,False)
    time.sleep(duration)
    return

def parse_flashes( num ):
    tens = int(num/10)
    mod = num % 10

    # Long flash every ten
    for i in range(tens):
        flash(0.6)

    # Short flash single units
    for i in range(mod):
        flash(0.2)

    return

if len(sys.argv) > 1:
    if isinstance(int(sys.argv[1]), int):
        parse_flashes(int(sys.argv[1]))
    else:
        print("Error: argument must be an integer.")
else:
    print("Error: I need an argument.")

GPIO.cleanup()
